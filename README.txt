********************************************
*  SPC TO CSV CONVERTER                    *
*  Ver. 0.0.1                              *
*  For PTR-MS SPC at UOL ONLY              *
********************************************

1. Note
This version of SPC to CSV file converter is specifically tailored to fit the specifications of PTR-MS SPC at University of Leicester Department of Chemistry ONLY. It might not produce correct conversion on other SPC files.

If you do require addtional features to be added to this tool, or that you have spotted any error or bug, please contact bo.zhao@leicester.ac.uk.

2. Usage
This is a GUI based tool, i.e. an user interface is provided. Command line usage can be added if required.

********************************************
2.1 Parameters

This tool has preset with the following default parameters:
|------------------------------------------|
| Mass range: 0 ~ 230                      |
| Mass resolution: 0.05                    |
| Peak range: -0.35 ~ +0.4                 |
|------------------------------------------|

Peak range explain:

The machine provides continuous readings of m/z counts based on mass resolution (e.g. 0, 0.05, 0.10, ... ). However, only with a certain range around those integer peak point will be extracted into the output result.

The current default settings for the peak range is -0.35 to +0.4, which means for peak point 19, m/z count from 18.65 to 19.40 will be added up to make the m/z count for mass 19 in the output result.

Based on default mass resolution and peak range, by default this tool extracts 7 data points before the peak, 8 data points after the peak, and the peak data points itself.

2.2 Config File
This tool will create a spcReader.config.properties file at the same directory with the JAR. This config file will allow you to edit these parameters as described in 2.1. On startup of the tool, it will first check whether there exists a config file, and if yes load parameters from the file.

On every convert, the tool will update this config file with latest config if adjusted on the interface. The tool will also save latest file source directory and output destination in this config file.

2.3 Output Path
By default, if there is no output path specified, the output files will be at the same directory as the source file.

2.4 Log File
This tool will create a spcReader.log file at the same directory with the JAR. This config file will log tool start, conversion process, input and output, tool exit with a timestamp. This will allow user to figure out which file has been converted and where it has been saved to, if necessary.

3. Accuracy
This tool has only been tested on two PTR-MS SPC files so far. There might be some errors and bugs. Please DO test this tool carefully with several additional SPC files before put in to actual use.
