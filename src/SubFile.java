import java.util.ArrayList;

/**
 * Created by bozhao on 18/11/2016.
 */
public class SubFile {
    private SubFileHeader header;
    private ArrayList x;
    private ArrayList y;

    private ArrayList peakX;

    public ArrayList getPeakX() {
        return peakX;
    }

    public void setPeakX(ArrayList peakX) {
        this.peakX = peakX;
    }

    public ArrayList getPeakY() {
        return peakY;
    }

    public void setPeakY(ArrayList peakY) {
        this.peakY = peakY;
    }

    private ArrayList peakY;

    public void printHeader() {
        header.printHeader();
    }

    public void printX() {
        System.out.println("------------------------------");
        System.out.println("Subfile X Data:");
        System.out.println(x);
    }

    public void printY() {
        System.out.println("------------------------------");
        System.out.println("Subfile Y Data:");
        System.out.println(y);
    }

    public void printPeakY() {
        System.out.println("------------------------------");
        System.out.println("Subfile Peak Y Data:");
        System.out.println(peakY);
    }

    public SubFileHeader getHeader() {
        return header;
    }

    public void setHeader(SubFileHeader header) {
        this.header = header;
    }

    public ArrayList getX() {
        return x;
    }

    public void setX(ArrayList x) {
        this.x = x;
    }

    public ArrayList getY() {
        return y;
    }

    public void setY(ArrayList y) {
        this.y = y;
    }
}
