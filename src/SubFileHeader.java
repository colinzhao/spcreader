/**
 * Created by bozhao on 18/11/2016.
 */
public class SubFileHeader {
    //subheader format: <cchfffiif4s
    private String subflgs;
    private int subexp;
    private int subindx;
    private float subtime;
    private float subnext;
    private float subnois;
    private int subnpts;
    private int subscan;
    private float subwlevel;
    private String subresv;

    public void printHeader () {
        System.out.println("------------------------------");
        System.out.println("Subfile Header Information:");
        System.out.println("subflgs: " + subflgs);
        System.out.println("subexp: " + subexp);
        System.out.println("subindx: " + subexp);
        System.out.println("subtime: " + subtime);
        System.out.println("subnext: " + subnext);
        System.out.println("subnois: " + subnois);
        System.out.println("subnpts: " + subnpts);
        System.out.println("subscan: " + subscan);
        System.out.println("subwlevel: " + subwlevel);
        System.out.println("subresv: " + subresv);
    }

    public String getSubflgs() {
        return subflgs;
    }

    public void setSubflgs(String subflgs) {
        this.subflgs = subflgs;
    }

    public int getSubexp() {
        return subexp;
    }

    public void setSubexp(int subexp) {
        this.subexp = subexp;
    }

    public int getSubindx() {
        return subindx;
    }

    public void setSubindx(int subindx) {
        this.subindx = subindx;
    }

    public float getSubtime() {
        return subtime;
    }

    public void setSubtime(float subtime) {
        this.subtime = subtime;
    }

    public float getSubnext() {
        return subnext;
    }

    public void setSubnext(float subnext) {
        this.subnext = subnext;
    }

    public float getSubnois() {
        return subnois;
    }

    public void setSubnois(float subnois) {
        this.subnois = subnois;
    }

    public int getSubnpts() {
        return subnpts;
    }

    public void setSubnpts(int subnpts) {
        this.subnpts = subnpts;
    }

    public int getSubscan() {
        return subscan;
    }

    public void setSubscan(int subscan) {
        this.subscan = subscan;
    }

    public float getSubwlevel() {
        return subwlevel;
    }

    public void setSubwlevel(float subwlevel) {
        this.subwlevel = subwlevel;
    }

    public String getSubresv() {
        return subresv;
    }

    public void setSubresv(String subresv) {
        this.subresv = subresv;
    }
}
