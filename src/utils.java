import org.apache.commons.lang3.Conversion;
import params.general;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by bozhao on 16/11/2016.
 */
public class utils {

    public static double readDouble(byte[] byteArray, int start, int length) {
        StringBuffer hexStringBuffer = new StringBuffer("");
        double doubleOutput = 0.0;
        if (general.endianType.equals(ByteOrder.LITTLE_ENDIAN)) {
            // little endian
            int end = start + length - 1;
            for (int i = end; i >= start; i--) {
                int unsignedInt = getUnsignedByte(byteArray[i]);
                if (unsignedInt < 16) {
                    hexStringBuffer.append("0").append(Integer.toHexString(unsignedInt));
                } else {
                    hexStringBuffer.append(Integer.toHexString(unsignedInt));
                }
            }
            String hexString = hexStringBuffer.toString();
            long longValue = new BigInteger(hexString, 16).longValue();
            doubleOutput = Double.longBitsToDouble(longValue);
        } else if (general.endianType.equals(ByteOrder.BIG_ENDIAN)) {
            // big endian
            int end = start + length - 1;
            for (int i = start; i <= end; i++) {
                int unsignedInt = getUnsignedByte(byteArray[i]);
                if (unsignedInt < 16) {
                    hexStringBuffer.append("0").append(Integer.toHexString(unsignedInt));
                } else {
                    hexStringBuffer.append(Integer.toHexString(unsignedInt));
                }
            }
            String hexString = hexStringBuffer.toString();
            long longValue = new BigInteger(hexString, 16).longValue();
            doubleOutput = Double.longBitsToDouble(longValue);
        }
        return doubleOutput;
    }

    public static int readInt (byte[] byteArray, int start, int length) {
        StringBuffer hexStringBuffer = new StringBuffer("");
        int intOutput = 0;
        if (general.endianType.equals(ByteOrder.LITTLE_ENDIAN)) {
            // little endian
            int end = start + length - 1;
            for (int i = end; i >= start; i--) {
                int unsignedInt = getUnsignedByte(byteArray[i]);
                if (unsignedInt < 16) {
                    hexStringBuffer.append("0").append(Integer.toHexString(unsignedInt));
                } else {
                    hexStringBuffer.append(Integer.toHexString(unsignedInt));
                }
            }
            String hexString = hexStringBuffer.toString();
            intOutput = Integer.parseInt(hexString, 16);
        } else if (general.endianType.equals(ByteOrder.BIG_ENDIAN)) {
            // big endian
            int end = start + length - 1;
            for (int i = start; i <= end; i++) {
                int unsignedInt = getUnsignedByte(byteArray[i]);
                if (unsignedInt < 16) {
                    hexStringBuffer.append("0").append(Integer.toHexString(unsignedInt));
                } else {
                    hexStringBuffer.append(Integer.toHexString(unsignedInt));
                }
            }
            String hexString = hexStringBuffer.toString();
            intOutput = Integer.parseInt(hexString, 16);
        }
        return intOutput;
    }

    public static float readFloat(byte[] byteArray, int start, int length) {
        StringBuffer hexStringBuffer = new StringBuffer("");
        float floatOutput = 0.0f;
        if (general.endianType.equals(ByteOrder.LITTLE_ENDIAN)) {
            // little endian
            int end = start + length - 1;
            for (int i = end; i >= start; i--) {
                int unsignedInt = getUnsignedByte(byteArray[i]);
                if (unsignedInt < 16) {
                    hexStringBuffer.append("0").append(Integer.toHexString(unsignedInt));
                } else {
                    hexStringBuffer.append(Integer.toHexString(unsignedInt));
                }
            }
            String hexString = hexStringBuffer.toString();
            Long longValue = Long.parseLong(hexString, 16);
            floatOutput = Float.intBitsToFloat(longValue.intValue());
        } else if (general.endianType.equals(ByteOrder.BIG_ENDIAN)) {
            // big endian
            int end = start + length - 1;
            for (int i = start; i <= end; i++) {
                int unsignedInt = getUnsignedByte(byteArray[i]);
                if (unsignedInt < 16) {
                    hexStringBuffer.append("0").append(Integer.toHexString(unsignedInt));
                } else {
                    hexStringBuffer.append(Integer.toHexString(unsignedInt));
                }
            }
            String hexString = hexStringBuffer.toString();
            Long longValue = Long.parseLong(hexString, 16);
            floatOutput = Float.intBitsToFloat(longValue.intValue());
        }
        return floatOutput;
    }

    public static String readString (byte[] byteArray, int start, int length) {
        CharBuffer charBuffer = CharBuffer.allocate(length);
        String stringOutput = "";
        for (int i = start; i < start + length; i++) {
            charBuffer.append((char) byteArray[i]);
        }
        stringOutput = charBuffer.toString();
        return stringOutput;
    }

    public static ArrayList<Boolean> readFlagBits (byte[] byteArray) {
        int unsignedFlagInt = getUnsignedByte(byteArray[0]);
        String flagBits = Integer.toBinaryString(unsignedFlagInt);
        char[] flagBitsArray = flagBits.toCharArray();
        //String reversedFlagBits = new StringBuilder(flagBits).reverse().toString();
        //char[] reversedFlagBitsArray = reversedFlagBits.toCharArray();
        // System.out.println("reversedFlagBits: " + reversedFlagBits);
        int lengthGap = 8 - flagBitsArray.length;
        ArrayList<Boolean> flagBitsList = new ArrayList();
        if (lengthGap > 0 ) {
            for (int i = 0; i < lengthGap; i++ ) {
                flagBitsList.add(false);
            }
        }
        for (int i = 0; i < flagBitsArray.length; i++) {
            switch (Character.getNumericValue(flagBitsArray[i])) {
                case 0:
                    flagBitsList.add(false);
                    break;
                case 1:
                    flagBitsList.add(true);
            }
        }
        //System.out.println("Before reverse: " + flagBitsList);
        Collections.reverse(flagBitsList);
        //System.out.println("After reverse: " + flagBitsList);
        return flagBitsList;
    }

    public static int getUnsignedByte (int signedByte) {
        return signedByte & 0xFF;
    }

    public static BigDecimal roundDouble(double d, int numberOfDecimal) {
        BigDecimal bigDecimal = new BigDecimal(Double.toString(d));
        bigDecimal = bigDecimal.setScale(numberOfDecimal, BigDecimal.ROUND_HALF_UP);
        return bigDecimal;
    }

    public static int getInt(byte[] bytes, int start, int length) {
        byte[] input = Arrays.copyOfRange(bytes, start, start + length);
        ByteBuffer buffer = ByteBuffer.wrap(input);
        int output = buffer.getShort();
        return output;
    }

    public static void main(String[] args) throws Exception{
        byte[] byteArray = readSPC.toByteArray("data/test.SPC");
        System.out.println(Conversion.byteArrayToInt(byteArray, 1, 0, 0, 1));
        System.out.println("-------------");
        System.out.println(readInt(byteArray, 4, 4));
        System.out.println(readFloat(byteArray, 312, 4));
        System.out.println(readInt(byteArray, 12, 4));
        System.out.println(readInt(byteArray, 16, 4));

        System.out.println(Conversion.byteArrayToInt(byteArray, 4, 9999, 0, 4));
        System.out.println(Float.intBitsToFloat(Conversion.byteArrayToInt(byteArray, 312, 9999, 0, 4)));
        boolean[] b = new boolean[8];
        System.out.println(Conversion.byteToBinary(byteArray[0], 0, b, 0, 8));
        for (int i = 0; i < b.length; i++) {
            System.out.println(b[i]);
        }
        System.out.println("-------------");
        ArrayList<Boolean> flagBits = new ArrayList(utils.readFlagBits(byteArray));
        for (boolean x : flagBits) {
            System.out.println(x);
        }

        Byte x = byteArray[1];
        System.out.println(getInt(byteArray, 4, 4));
        byte bbb = (byte) 0x80;
        System.out.println(bbb);
    }
}
