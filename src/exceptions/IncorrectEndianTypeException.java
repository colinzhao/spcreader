package exceptions;

/**
 * Created by bozhao on 16/11/2016.
 */
public class IncorrectEndianTypeException extends Exception {
    private String endianType;
    
    public IncorrectEndianTypeException(String endianType) {
        this.endianType = endianType;
    }

    public String getEndianType() {
        return endianType;
    }
}
