import javax.swing.*;
import java.awt.event.*;

public class ConversionDone extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JList listInputFiles;
    private JList listOutputFiles;

    public ConversionDone() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        listInputFiles.setListData(spcReaderGUI.selectedFiles);
        listOutputFiles.setListData(spcReaderGUI.outputFiles.toArray());

    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        ConversionDone dialog = new ConversionDone();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
