import javax.swing.*;
import java.awt.event.*;

import params.configurable;

public class VersionInfo extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JLabel labelVersionInfo;

    public VersionInfo() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        labelVersionInfo.setText("<html>" +
                "<div style=\"width: 300px;\">" +
                "<p>This SPC to CSV file converter has been specifically tailored to work with PTR-MS cart at University of Leicester Department of Chemistry.</p><br>" +
                "<p>A full general SPC compatible version would be avialable in reasonable future.</p><br>" +
                "<p>If you do require additional features at this stage or that you have spotted any error, please contact <a href=\"mailto:bo.zhao@leicester.ac.uk\">bo.zhao@leicester.ac.uk</a>.</p>" +
                "</div>" +
                "</html>");

    }

    private void onOK() {
        // add your code here
        configurable.firstRun = false;
        dispose();
    }

    public static void main(String[] args) {
        VersionInfo dialog = new VersionInfo();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
