package params;

/**
 * Created by bozhao on 21/11/2016.
 */

public class configurable {
    // peak range from -0.35 ~ + 0.4
    // e.g. for mass 100, data start from 99.65(included) to 100.4(included) will be included.


    public static int massRangeStart = 0;
    public static int massRangeEnd = 230;
    public static double massResolution = 0.05;

    public static double peakStart = 0.35;
    public static int dataBeforePeak = (int) Math.round(0.35 / massResolution);
    public static double peakEnd = 0.4;
    public static int dataAfterPeak = (int) Math.round(0.4 / massResolution);

    public static int massPeakGap = (int) (1 / massResolution);
    public static String lastOutput = "";
    public static String lastOpened = "";
    public static boolean firstRun = true;
}
