import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import params.configurable;

/**
 * Created by bozhao on 21/11/2016.
 */
public class spcReaderGUI extends JFrame {
    private JButton btnLoadFiles;
    private JPanel contentPane;
    private JButton btnConvert;
    private JLabel labelToolName;
    private JList listSelectedFiles;
    private JButton btnExit;
    private JLabel labelParametersNote;
    private JTextField inputMassRangeMin;
    private JTextField inputMassRangeMax;
    private JTextField inputMassResolution;
    private JTextField inputRangeBeforePeak;
    private JTextField inputRangeAfterPeak;
    private JButton btnSelectOutput;
    private JTextField inputOutputPath;
    private JLabel labelOutputNote;

    public static File[] selectedFiles;
    public static ArrayList outputFiles;
    private String outputPath;
    private Logger log;

    public spcReaderGUI() {
        log = LogManager.getRootLogger();
        setContentPane(contentPane);

        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yy");

        log.info("SPC to CSV Converter");
        log.info(dateFormat.format(new Date()));

        readSPC.loadProp();

        if (configurable.firstRun) {
            log.info("**********************************");
            log.info("Default parameters");
            log.info("----------------------------------");
        } else {
            log.info("**********************************");
            log.info("Previous parameters");
            log.info("----------------------------------");
        }
        log.info("Mass Range: " + configurable.massRangeStart + " ~ " + configurable.massRangeEnd);
        log.info("Mass Resolution: " + configurable.massResolution);
        log.info("Peak Range: peak-" + configurable.peakStart + " ~ peak+" + configurable.peakEnd);

        labelToolName.setText("SPC to CSV Converter for PTR-MS SPC File");
        labelParametersNote.setText("These parameters have already been configured by default to match the PTR-MS cart specifications.");
        labelOutputNote.setText("If no output path being specified, the output file(s) will be at the same directory as the source file.");

        inputMassRangeMin.setText(String.valueOf(configurable.massRangeStart));
        inputMassRangeMax.setText(String.valueOf(configurable.massRangeEnd));
        inputMassResolution.setText(String.valueOf(configurable.massResolution));

        inputRangeBeforePeak.setText(String.valueOf(configurable.peakStart));
        inputRangeAfterPeak.setText(String.valueOf(configurable.peakEnd));

        inputOutputPath.setText(configurable.lastOutput);
        outputPath = configurable.lastOutput;

        if (configurable.firstRun) {
            VersionInfo versionInfoDialog = new VersionInfo();
            versionInfoDialog.pack();
            versionInfoDialog.setResizable(false);
            versionInfoDialog.setLocationRelativeTo(null);
            versionInfoDialog.setVisible(true);
        }

        inputMassRangeMin.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
                char vchar = keyEvent.getKeyChar();
                if (!(Character.isDigit(vchar) || vchar == KeyEvent.VK_BACK_SPACE || vchar == KeyEvent.VK_DELETE)) {
                    keyEvent.consume();
                }
            }
        });
        inputMassRangeMax.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
                char vchar = keyEvent.getKeyChar();
                if (!(Character.isDigit(vchar) || vchar == KeyEvent.VK_BACK_SPACE || vchar == KeyEvent.VK_DELETE)) {
                    keyEvent.consume();
                }
            }
        });
        inputMassResolution.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
                char vchar = keyEvent.getKeyChar();
                if (!(Character.isDigit(vchar) || vchar == KeyEvent.VK_BACK_SPACE || vchar == KeyEvent.VK_DELETE || vchar == KeyEvent.VK_PERIOD)) {
                    keyEvent.consume();
                }
            }
        });
        inputRangeBeforePeak.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
                char vchar = keyEvent.getKeyChar();
                if (!(Character.isDigit(vchar) || vchar == KeyEvent.VK_BACK_SPACE || vchar == KeyEvent.VK_DELETE || vchar == KeyEvent.VK_PERIOD)) {
                    keyEvent.consume();
                }
            }
        });
        inputRangeAfterPeak.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
                char vchar = keyEvent.getKeyChar();
                if (!(Character.isDigit(vchar) || vchar == KeyEvent.VK_BACK_SPACE || vchar == KeyEvent.VK_DELETE || vchar == KeyEvent.VK_PERIOD)) {
                    keyEvent.consume();
                }
            }
        });
        btnLoadFiles.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser loadFiles = new JFileChooser(configurable.lastOpened);

                FileNameExtensionFilter spcFileOnlyFilter = new FileNameExtensionFilter("SPC File", "spc");
                loadFiles.setFileFilter(spcFileOnlyFilter);

                loadFiles.setFileSelectionMode(JFileChooser.FILES_ONLY);

                loadFiles.setMultiSelectionEnabled(true);
                loadFiles.showOpenDialog(null);

                selectedFiles = loadFiles.getSelectedFiles();
                listSelectedFiles.setListData(selectedFiles);

                configurable.lastOpened = loadFiles.getCurrentDirectory().getAbsolutePath();
            }
        });
        btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                log.exit("Exit!");
                dispose();
            }
        });
        btnConvert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // TODO convert selected file(s)
                outputPath = inputOutputPath.getText();
                configurable.massRangeStart = Integer.parseInt(inputMassRangeMin.getText());
                configurable.massRangeEnd = Integer.parseInt(inputMassRangeMax.getText());
                configurable.massResolution = Double.parseDouble(inputMassResolution.getText());
                configurable.peakStart = Double.parseDouble(inputRangeBeforePeak.getText());
                configurable.peakEnd = Double.parseDouble(inputRangeAfterPeak.getText());
                configurable.lastOutput = inputOutputPath.getText();
                readSPC.saveProp();
                outputFiles = new ArrayList();
                log.info("**********************************");
                log.info("Conversion start");
                log.info("----------------------------------");

                if (outputPath.equals("")) {
                    log.info("No output path specified");
                    for (File file : selectedFiles) {
                        String filePath = file.getAbsolutePath();
                        String savePath = FilenameUtils.removeExtension(filePath) + ".csv";
                        outputFiles.add(savePath);

                        System.out.println("--------------------");
                        System.out.println("Input: " + filePath);

                        log.info("----------------------------------");
                        log.info("Input: " + filePath);

                        readSPC.spcToCsv(filePath, savePath);
                        System.out.println("Output: " + savePath);
                        System.out.println("Done!");
                        log.info("----------------------------------");
                        log.info("Output: " + savePath);
                    }
                } else {
                    log.info("Output at: " + outputPath);
                    for (File file : selectedFiles) {
                        String fileName = file.getName();
                        String filePath = file.getAbsolutePath();
                        String savePath = outputPath + "/" + FilenameUtils.removeExtension(fileName) + ".csv";
                        outputFiles.add(savePath);

                        System.out.println("--------------------");
                        System.out.println("Input: " + filePath);
                        log.info("----------------------------------");
                        log.info("Input: " + filePath);

                        readSPC.spcToCsv(filePath, savePath);
                        System.out.println("Output: " + savePath);
                        System.out.println("Done!");
                        log.info("----------------------------------");
                        log.info("Output: " + savePath);
                    }
                }
                log.info("----------------------------------");
                log.info("Done!");
                ConversionDone dialogDone = new ConversionDone();
                dialogDone.pack();
                dialogDone.setResizable(false);
                dialogDone.setLocationRelativeTo(null);
                dialogDone.setVisible(true);
            }
        });
        btnSelectOutput.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // TODO set select output path folder
                JFileChooser setOutputFolder = new JFileChooser(configurable.lastOutput);
                setOutputFolder.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                setOutputFolder.showOpenDialog(null);
                inputOutputPath.setText(setOutputFolder.getSelectedFile().getAbsolutePath());
            }
        });
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
            UIManager.put("swing.boldMetal", Boolean.FALSE);

        } catch (Exception e) {
            e.printStackTrace();
        }

        spcReaderGUI dialog = new spcReaderGUI();
        dialog.pack();
        dialog.setResizable(false);
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
    }


    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
